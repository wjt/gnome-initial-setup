# Dutch translation for gnome-initial-setup.
# This file is distributed under the same license as the gnome-initial-setup package.
# enterprise login - Hannie: enterprise-aanmelding vervangen door bedrijfsaanmelding
# brentx <deweerdt.brent@gmail.com>, 2013.
# Reinout van Schouwen <reinouts@gnome.org>, 2013, 2014.
# Wouter Bolsterlee <wbolster@gnome.org>, 2013.
# Erwin Poeze <donnut@outlook.com>, 2013.
# Nathan Follens <nthn@unseen.is>, 2015-2020.
# Hannie Dumoleyn <hannie@ubuntu-nl.org>, 2014, 2015, 2018.
# Justin van Steijn <jvs@fsfe.org>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: gnome-initial-setup\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-initial-setup/"
"issues\n"
"POT-Creation-Date: 2020-02-14 16:48+0000\n"
"PO-Revision-Date: 2020-02-24 21:33+0100\n"
"Last-Translator: Nathan Follens <nthn@unseen.is>\n"
"Language-Team: Dutch <gnome-nl-list@gnome.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.3\n"
"X-Project-Style: gnome\n"

#: data/gnome-initial-setup-first-login.desktop.in.in:3
#: data/gnome-initial-setup.desktop.in.in:3
msgid "Initial Setup"
msgstr "Instellingen voor eerste gebruik"

#: gnome-initial-setup/gis-assistant.c:419
msgid "_Next"
msgstr "Volge_nde"

#: gnome-initial-setup/gis-assistant.c:420
msgid "_Accept"
msgstr "_Accepteren"

#: gnome-initial-setup/gis-assistant.c:421
msgid "_Skip"
msgstr "Over_slaan"

#: gnome-initial-setup/gis-assistant.c:422
msgid "_Previous"
msgstr "_Vorige"

#: gnome-initial-setup/gis-assistant.c:423
msgid "_Cancel"
msgstr "_Annuleren"

#: gnome-initial-setup/gnome-initial-setup.c:241
msgid "Force existing user mode"
msgstr "Bestaande gebruikersmodus forceren"

#: gnome-initial-setup/gnome-initial-setup.c:247
msgid "— GNOME initial setup"
msgstr "— Instellingen voor eerste gebruik van Gnome"

#: gnome-initial-setup/pages/account/gis-account-avatar-chooser.ui:36
msgid "Take a Picture…"
msgstr "Een foto maken…"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:184
msgid "Failed to register account"
msgstr "Registreren van account is mislukt"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:378
msgid "No supported way to authenticate with this domain"
msgstr "Authenticatie met dit domein is niet ondersteund"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:418
msgid "Failed to join domain"
msgstr "Lid worden van domein is mislukt"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:486
msgid "Failed to log into domain"
msgstr "Aanmelden bij domein is mislukt"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:17
msgid "Enterprise Login"
msgstr "Bedrijfsaanmelding"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:18
msgid ""
"Enterprise login allows an existing centrally managed user account to be "
"used on this device. You can also use this account to access company "
"resources on the internet."
msgstr ""
"Met Bedrijfsaanmelding kunt u een bestaande, centraal beheerde "
"gebruikersaccount gebruiken op dit apparaat. U kan deze account ook "
"gebruiken om toegang te krijgen tot middelen van het bedrijf op het internet."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:36
#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:287
msgid "_Domain"
msgstr "_Domein"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:53
#: gnome-initial-setup/pages/account/gis-account-page-local.ui:84
msgid "_Username"
msgstr "Gebr_uikersnaam"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:70
#: gnome-initial-setup/pages/password/gis-password-page.ui:34
msgid "_Password"
msgstr "_Wachtwoord"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:146
msgid "Enterprise domain or realm name"
msgstr "Bedrijfsdomein– of realmnaam"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:211
msgid "C_ontinue"
msgstr "_Verder"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:246
msgid "Domain Administrator Login"
msgstr "Aanmelden domeinbeheerder"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:266
msgid ""
"In order to use enterprise logins, this computer needs to be enrolled in a "
"domain. Please have your network administrator type the domain password "
"here, and choose a unique computer name for your computer."
msgstr ""
"Om bedrijfsaanmeldingen te gebruiken moet deze computer ingeschreven zijn "
"bij een domein. Laat uw netwerkbeheerder het domeinwachtwoord invoeren en "
"kies een unieke naam voor uw computer."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:321
msgid "_Computer"
msgstr "_Computer"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:353
msgid "Administrator _Name"
msgstr "_Naam beheerder"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:387
msgid "Administrator Password"
msgstr "_Wachtwoord beheerder"

#: gnome-initial-setup/pages/account/gis-account-page-local.c:206
msgid "Please check the name and username. You can choose a picture too."
msgstr "Controleer de naam en gebruikersnaam. U kan ook een afbeelding kiezen."

#: gnome-initial-setup/pages/account/gis-account-page-local.c:455
msgid "We need a few details to complete setup."
msgstr "We hebben wat gegevens nodig om het instellen te voltooien."

#: gnome-initial-setup/pages/account/gis-account-page-local.c:562
msgid "Administrator"
msgstr "Beheerder"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:21
msgid "Avatar image"
msgstr "Gebruikersafbeelding"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:38
#: gnome-initial-setup/pages/account/gis-account-page.c:281
msgid "About You"
msgstr "Over u"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:39
msgid "Please provide a name and username. You can choose a picture too."
msgstr "Voer een naam en gebruikersnaam in. U kan ook een afbeelding kiezen."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:53
msgid "_Full Name"
msgstr "_Volledige naam"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:149
msgid "Set up _parental controls for this user"
msgstr "Stel ouderlijk toezicht in voor deze gebruiker"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:156
msgid "For use by a parent or supervisor, who must set up their own password."
msgstr ""
"Te gebruiken door een ouder of toezichthouder, die zelf een wachtwoord moet "
"instellen."

#: gnome-initial-setup/pages/account/gis-account-page.ui:43
msgid "_Enterprise Login"
msgstr "B_edrijfsaanmelding"

#: gnome-initial-setup/pages/account/gis-account-page.ui:53
msgid "Go online to set up Enterprise Login."
msgstr "Ga online om bedrijfsaanmelding in te stellen."

#: gnome-initial-setup/pages/account/um-realm-manager.c:310
msgid "Cannot automatically join this type of domain"
msgstr "Automatisch aanmelden bij dit type domein is niet mogelijk"

#: gnome-initial-setup/pages/account/um-realm-manager.c:373
#, c-format
msgid "No such domain or realm found"
msgstr "Dergelijk domein of realm niet gevonden"

#: gnome-initial-setup/pages/account/um-realm-manager.c:782
#: gnome-initial-setup/pages/account/um-realm-manager.c:796
#, c-format
msgid "Cannot log in as %s at the %s domain"
msgstr "Aanmelden als %s bij domein %s is mislukt"

#: gnome-initial-setup/pages/account/um-realm-manager.c:788
msgid "Invalid password, please try again"
msgstr "Ongeldig wachtwoord, probeer het opnieuw"

#: gnome-initial-setup/pages/account/um-realm-manager.c:801
#, c-format
msgid "Couldn’t connect to the %s domain: %s"
msgstr "Verbinding maken met het domein %s is mislukt: %s"

#: gnome-initial-setup/pages/account/um-utils.c:248
msgid "Sorry, that user name isn’t available. Please try another."
msgstr "Sorry, die gebruikersnaam is niet beschikbaar. Probeer een andere."

#: gnome-initial-setup/pages/account/um-utils.c:251
#, c-format
msgid "The username is too long."
msgstr "De gebruikersnaam is te lang."

#: gnome-initial-setup/pages/account/um-utils.c:254
msgid "The username cannot start with a “-”."
msgstr "De gebruikersnaam kan niet beginnen met een ‘-’."

#: gnome-initial-setup/pages/account/um-utils.c:257
msgid "That username isn’t available. Please try another."
msgstr "Die gebruikersnaam is niet beschikbaar. Probeer een andere."

#: gnome-initial-setup/pages/account/um-utils.c:260
msgid ""
"The username should only consist of upper and lower case letters from a-z, "
"digits and the following characters: . - _"
msgstr ""
"De gebruikersnaam mag alleen maar bestaan uit hoofdletters en kleine letters "
"van a t/m z, cijfers, en volgende tekens: . - _"

#: gnome-initial-setup/pages/account/um-utils.c:264
msgid "This will be used to name your home folder and can’t be changed."
msgstr ""
"Dit zal worden gebruikt als naam van uw thuismap en kan niet worden "
"gewijzigd."

#: gnome-initial-setup/pages/goa/gis-goa-page.c:92
msgid "Add Account"
msgstr "Account toevoegen"

#: gnome-initial-setup/pages/goa/gis-goa-page.c:357
msgid "Online Accounts"
msgstr "Online-accounts"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:17
msgid "Connect Your Online Accounts"
msgstr "Uw online-accounts koppelen"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:18
msgid ""
"Connect your accounts to easily access your email, online calendar, "
"contacts, documents and photos."
msgstr ""
"Door accounts te koppelen kun u eenvoudig verbinding maken met uw e-mail, "
"online agenda, contactpersonen en fotoboeken."

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:48
msgid ""
"Accounts can be added and removed at any time from the Settings application."
msgstr ""
"Accounts kunnen altijd toegevoegd en verwijderd worden via Instellingen."

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:240
msgid "Preview"
msgstr "Voorbeeld"

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:299
#: gnome-initial-setup/pages/language/cc-language-chooser.c:227
msgid "More…"
msgstr "Meer…"

#. Translators: a search for input methods or keyboard layouts
#. * did not yield any results
#.
#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:321
msgid "No inputs found"
msgstr "Geen invoer gevonden"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.c:482
#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:15
msgid "Typing"
msgstr "Typen"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:16
msgid "Select your keyboard layout or an input method."
msgstr "Selecteer uw toetsenbordindeling of een invoermethode."

#: gnome-initial-setup/pages/language/cc-language-chooser.c:243
msgid "No languages found"
msgstr "Geen talen gevonden"

#: gnome-initial-setup/pages/language/gis-language-page.c:262
msgid "Welcome"
msgstr "Welkom"

#. Translators: This is meant to be a warm, engaging welcome message,
#. * like greeting somebody at the door. If the exclamation mark is not
#. * suitable for this in your language you may replace it.
#.
#: gnome-initial-setup/pages/language/gis-welcome-widget.c:136
msgid "Welcome!"
msgstr "Welkom!"

#: gnome-initial-setup/pages/network/gis-network-page.c:318
msgctxt "Wireless access point"
msgid "Other…"
msgstr "Overige…"

# Dit komt beter over dan 'Draadloos
# netwerken is uitgeschakeld',
# waarbij het niet meteen duidelijk is
# dat het om het werkwoord
# 'netwerken' gaat, en niet het
# zelfstandige naamwoord - Nathan
#: gnome-initial-setup/pages/network/gis-network-page.c:403
msgid "Wireless networking is disabled"
msgstr "Draadloze netwerken zijn uitgeschakeld"

#: gnome-initial-setup/pages/network/gis-network-page.c:410
msgid "Checking for available wireless networks"
msgstr "Zoeken naar beschikbare draadloze netwerken"

#: gnome-initial-setup/pages/network/gis-network-page.c:805
msgid "Network"
msgstr "Netwerk"

#: gnome-initial-setup/pages/network/gis-network-page.ui:18
msgid "Wi-Fi"
msgstr "Wifi"

#: gnome-initial-setup/pages/network/gis-network-page.ui:19
msgid ""
"Connecting to the Internet will enable you to set the time, add your "
"details, and enable you to access your email, calendar, and contacts. It is "
"also necessary for enterprise login accounts."
msgstr ""
"Als er verbinding met het internet is, kunt u de tijd instellen, uw gegevens "
"toevoegen, en toegang krijgen tot uw e-mail, agenda en contactpersonen. "
"Verbinding is ook noodzakelijk voor bedrijfsaanmeldingen."

#: gnome-initial-setup/pages/network/gis-network-page.ui:76
msgid "No wireless available"
msgstr "Geen draadloos netwerk beschikbaar"

#: gnome-initial-setup/pages/network/gis-network-page.ui:91
msgid "Turn On"
msgstr "Inschakelen"

#. Translators: The placeholder is the user’s full name.
#: gnome-initial-setup/pages/parental-controls/gis-parental-controls-page.c:110
#, c-format
msgid "Parental Controls for %s"
msgstr "Ouderlijk toezicht voor %s"

#: gnome-initial-setup/pages/parental-controls/gis-parental-controls-page.c:112
msgid "Set restrictions on what this user can run or install."
msgstr "Beperk wat deze gebruiker kan uitvoeren of installeren."

#: gnome-initial-setup/pages/parental-controls/gis-parental-controls-page.c:207
msgid "Parental Controls"
msgstr "Ouderlijk toezicht"

#. Don’t break UI compatibility if parental controls are disabled.
#: gnome-initial-setup/pages/password/gis-password-page.c:79
msgid "Set a Password"
msgstr "Wachtwoord instellen"

#: gnome-initial-setup/pages/password/gis-password-page.c:80
#: gnome-initial-setup/pages/password/gis-password-page.c:89
msgid "Be careful not to lose your password."
msgstr "Zorg ervoor dat u uw wachtwoord niet verliest."

#. Translators: The placeholder is for the user’s full name.
#: gnome-initial-setup/pages/password/gis-password-page.c:87
#, c-format
msgid "Set a Password for %s"
msgstr "Stel een wachtwoord in voor %s"

#: gnome-initial-setup/pages/password/gis-password-page.c:95
msgid "Set a Parent Password"
msgstr "Stel een ouderwachtwoord in"

#. Translators: The placeholder is the full name of the child user on the system.
#: gnome-initial-setup/pages/password/gis-password-page.c:97
#, c-format
msgid "This password will control access to the parental controls for %s."
msgstr ""
"Dit wachtwoord zal de toegang tot de instellingen voor ouderlijk toezicht "
"voor %s beschermen."

#: gnome-initial-setup/pages/password/gis-password-page.c:233
msgid "This is a weak password."
msgstr "Dit is een zwak wachtwoord."

#: gnome-initial-setup/pages/password/gis-password-page.c:239
msgid "The passwords do not match."
msgstr "Wachtwoorden komen niet overeen."

#: gnome-initial-setup/pages/password/gis-password-page.c:425
msgid "Password"
msgstr "Wachtwoord"

#: gnome-initial-setup/pages/password/gis-password-page.ui:66
msgid "_Confirm"
msgstr "_Bevestigen"

#: gnome-initial-setup/pages/password/pw-utils.c:81
msgctxt "Password hint"
msgid "The new password needs to be different from the old one."
msgstr "Het nieuwe wachtwoord mag niet hetzelfde zijn als het oude."

#: gnome-initial-setup/pages/password/pw-utils.c:83
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing some letters "
"and numbers."
msgstr ""
"Dit wachtwoord lijkt erg op uw laatste wachtwoord. Probeer een paar letters "
"en cijfers te wijzigen."

#: gnome-initial-setup/pages/password/pw-utils.c:85
#: gnome-initial-setup/pages/password/pw-utils.c:93
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing the password a "
"bit more."
msgstr ""
"Dit wachtwoord lijkt erg op uw laatste wachtwoord. Probeer het wachtwoord "
"iets meer te wijzigen."

#: gnome-initial-setup/pages/password/pw-utils.c:87
msgctxt "Password hint"
msgid ""
"This is a weak password. A password without your user name would be stronger."
msgstr ""
"Dit is een zwak wachtwoord. Een wachtwoord zonder uw gebruikersnaam zou "
"sterker zijn."

#: gnome-initial-setup/pages/password/pw-utils.c:89
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid using your name in the password."
msgstr ""
"Dit is een zwak wachtwoord. Probeer het gebruik van uw naam in het "
"wachtwoord te vermijden."

#: gnome-initial-setup/pages/password/pw-utils.c:91
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid some of the words included in the "
"password."
msgstr ""
"Dit is een zwak wachtwoord. Probeer enkele van de woorden die in het "
"wachtwoord voorkomen te vermijden."

#: gnome-initial-setup/pages/password/pw-utils.c:95
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid common words."
msgstr "Dit is een zwak wachtwoord. Probeer algemene woorden te vermijden."

#: gnome-initial-setup/pages/password/pw-utils.c:97
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid reordering existing words."
msgstr ""
"Dit is een zwak wachtwoord. Probeer het herschikken van bestaande woorden te "
"vermijden."

#: gnome-initial-setup/pages/password/pw-utils.c:99
msgctxt "Password hint"
msgid "This is a weak password. Try to use more numbers."
msgstr "Dit is een zwak wachtwoord. Probeer meer cijfers te gebruiken."

#: gnome-initial-setup/pages/password/pw-utils.c:101
msgctxt "Password hint"
msgid "This is a weak password. Try to use more uppercase letters."
msgstr "Dit is een zwak wachtwoord. Probeer meer hoofdletters te gebruiken."

#: gnome-initial-setup/pages/password/pw-utils.c:103
msgctxt "Password hint"
msgid "This is a weak password. Try to use more lowercase letters."
msgstr "Dit is een zwak wachtwoord. Probeer meer kleine letters te gebruiken."

#: gnome-initial-setup/pages/password/pw-utils.c:105
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use more special characters, like "
"punctuation."
msgstr ""
"Dit is een zwak wachtwoord. Probeer meer speciale tekens, zoals leestekens, "
"te gebruiken."

#: gnome-initial-setup/pages/password/pw-utils.c:107
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use a mixture of letters, numbers and "
"punctuation."
msgstr ""
"Dit is een zwak wachtwoord. Probeer een combinatie van letters, cijfers en "
"leestekens te gebruiken."

#: gnome-initial-setup/pages/password/pw-utils.c:109
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid repeating the same character."
msgstr ""
"Dit is een zwak wachtwoord. Probeer herhaling van hetzelfde teken te "
"vermijden."

#: gnome-initial-setup/pages/password/pw-utils.c:111
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid repeating the same type of character: "
"you need to mix up letters, numbers and punctuation."
msgstr ""
"Dit is een zwak wachtwoord. Probeer herhaling van hetzelfde soort teken te "
"vermijden: u dient letters, cijfers en leestekens met elkaar te combineren."

#: gnome-initial-setup/pages/password/pw-utils.c:113
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid sequences like 1234 or abcd."
msgstr ""
"Dit is een zwak wachtwoord. Probeer reeksen zoals 1234 of abcd te vermijden."

#: gnome-initial-setup/pages/password/pw-utils.c:115
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to add more letters, numbers and punctuation."
msgstr ""
"Dit is een zwak wachtwoord. Probeer meer letters, cijfers en leestekens toe "
"te voegen."

#: gnome-initial-setup/pages/password/pw-utils.c:117
msgctxt "Password hint"
msgid "Mix uppercase and lowercase and try to use a number or two."
msgstr "Meng hoofdletters en kleine letters en gebruik een paar cijfers."

#: gnome-initial-setup/pages/password/pw-utils.c:119
msgctxt "Password hint"
msgid ""
"Adding more letters, numbers and punctuation will make the password stronger."
msgstr ""
"Het toevoegen van meer letters, cijfers en leestekens zal het wachtwoord nog "
"sterker maken."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:68
#, c-format
msgid ""
"Sending reports of technical problems helps us to improve %s. Reports are "
"sent anonymously and are scrubbed of personal data."
msgstr ""
"Het melden van technische problemen helpt ons %s te verbeteren. Meldingen "
"worden anoniem verzonden en persoonlijke gegevens worden eruit gewist."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:78
#, c-format
msgid "Problem data will be collected by %s:"
msgstr "Probleemgegevens zullen worden verzameld door %s:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:79
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:135
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:210
msgid "Privacy Policy"
msgstr "Privacybeleid"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:135
msgid "Uses Mozilla Location Service:"
msgstr "Gebruikt Mozilla-locatiediensten:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:246
#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:16
msgid "Privacy"
msgstr "Privacy"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:31
msgid "Location Services"
msgstr "Locatiediensten"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:52
msgid ""
"Allows applications to determine your geographical location. An indication "
"is shown when location services are in use."
msgstr ""
"Laat toepassingen toe uw geografische locatie te bepalen. Wanneer "
"locatiediensten in gebruik zijn, wordt dit aangegeven."

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:74
msgid "Automatic Problem Reporting"
msgstr "Automatische probleemrapportage"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:110
msgid ""
"Privacy controls can be changed at any time from the Settings application."
msgstr "Privacyvoorkeuren kunnen altijd gewijzigd worden via Instellingen."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME 3" if we can't
#. * detect any distribution.
#: gnome-initial-setup/pages/summary/gis-summary-page.c:227
#, c-format
msgid "_Start Using %s"
msgstr "_Beginnen met %s"

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME 3" if we can't
#. * detect any distribution.
#: gnome-initial-setup/pages/summary/gis-summary-page.c:234
#, c-format
msgid "%s is ready to be used. We hope that you love it!"
msgstr "%s is klaar om te gebruiken. We hopen dat het u bevalt!"

#: gnome-initial-setup/pages/summary/gis-summary-page.c:258
msgid "Setup Complete"
msgstr "Installatie voltooid"

#: gnome-initial-setup/pages/summary/gis-summary-page.ui:70
msgid "All done!"
msgstr "Klaar!"

#. Translators: "city, country"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:261
#, c-format
msgctxt "timezone loc"
msgid "%s, %s"
msgstr "%s, %s"

#. Translators: UTC here means the Coordinated Universal Time.
#. * %:::z will be replaced by the offset from UTC e.g. UTC+02
#.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:298
msgid "UTC%:::z"
msgstr "UTC%:::z"

#. Translators: This is the time format used in 12-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:302
msgid "%l:%M %p"
msgstr "%l:%M %p"

#. Translators: This is the time format used in 24-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:305
msgid "%R"
msgstr "%R"

#. Translators: "timezone (utc shift)"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:308
#, c-format
msgctxt "timezone map"
msgid "%s (%s)"
msgstr "%s (%s)"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:462
#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:17
msgid "Time Zone"
msgstr "Tijdzone"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:18
msgid ""
"The time zone will be set automatically if your location can be found. You "
"can also search for a city to set it yourself."
msgstr ""
"Indien uw locatie niet gevonden kan worden, wordt de tijdzone automatisch "
"ingesteld. U kan ook zelf een stad zoeken en deze instellen."

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:56
msgid "Please search for a nearby city"
msgstr "Zoek de dichtstbijgelegen stad"

#~ msgid "preferences-system"
#~ msgstr "voorkeuren-systeem"

#~ msgid "License Agreements"
#~ msgstr "Licentieovereenkomsten"

#~ msgid ""
#~ "I have _agreed to the terms and conditions in this end user license "
#~ "agreement."
#~ msgstr ""
#~ "Ik ga akkoord met de voorwaarden en bepalingen in deze "
#~ "licentieovereenkomst voor eindgebruikers."

#~ msgid "No regions found"
#~ msgstr "Geen regio’s gevonden"

#~ msgid "Region"
#~ msgstr "Regio"

#~ msgid "Choose your country or region."
#~ msgstr "Kies uw land of regio."

#~ msgid "Software Repositories"
#~ msgstr "Softwarebronnen"

#~ msgid "Access additional software from selected third party sources."
#~ msgstr ""
#~ "Toegang krijgen tot aanvullende software van geselecteerde derde partijen."

#~ msgid ""
#~ "Some of this software is proprietary and therefore has restrictions on "
#~ "use, sharing, and access to source code."
#~ msgstr ""
#~ "Deze software is niet-vrij en er heeft daarom beperkingen met betrekking "
#~ "tot het gebruik, verspreiding, en toegang tot de broncode."

#~ msgid "Additional Software Repositories"
#~ msgstr "Aanvullende softwarebronnen"

#~ msgid "<a href=\"more\">Find out more…</a>"
#~ msgstr "<a href=\"more\">Lees meer…</a>"

#~ msgid "Third Party Repositories"
#~ msgstr "Softwarebronnen van derden"

#~ msgid ""
#~ "Proprietary software typically has restrictions on how it can be used and "
#~ "on access to source code. This prevents anyone but the software owner "
#~ "from inspecting, improving or learning from its code."
#~ msgstr ""
#~ "Niet-vrije software heeft doorgaans beperkingen op het gebruik van en de "
#~ "toegang tot broncode. Dit verhindert iedereen behalve de eigenaar van de "
#~ "software deze code te kunnen controleren, verbeteren of er van te leren."

#~ msgid ""
#~ "In contrast, Free Software can be freely run, copied, distributed, "
#~ "studied and modified."
#~ msgstr ""
#~ "Vrije software daarentegen kan vrij uitgevoerd, verspreid, bestudeerd en "
#~ "aangepast worden."

#~ msgid "Ready to Go"
#~ msgstr "Klaar om verder te gaan"

#~ msgid "You’re ready to go!"
#~ msgstr "U bent klaar om te beginnen!"

#~ msgid "Disable image"
#~ msgstr "Afbeelding uitschakelen"

#~ msgctxt "Password hint"
#~ msgid "Try to avoid common words."
#~ msgstr "Probeer algemene woorden te vermijden."

#~ msgctxt "Password hint"
#~ msgid "Try to use more numbers."
#~ msgstr "Probeer meer cijfers te gebruiken."

#~ msgctxt "Password hint"
#~ msgid ""
#~ "Password needs to be longer. Try to add more letters, numbers and "
#~ "punctuation."
#~ msgstr ""
#~ "Probeer een combinatie van letters, cijfers en leestekens te gebruiken."

#~ msgid ""
#~ "Proprietary software sources provide access to additional software, "
#~ "including web browsers and games. This software typically has "
#~ "restrictions on use and access to source code, and is not provided by %s."
#~ msgstr ""
#~ "Propriëtaire softwarebronnen bieden toegang tot extra software, inclusief "
#~ "webbrowsers en games. Deze software heeft doorgaans beperkingen op het "
#~ "gebruik van en de toegang tot broncode, en wordt niet aangeboden door %s."

#~ msgid "Proprietary Software Sources"
#~ msgstr "Propriëtaire softwarebronnen"

#~ msgid "A user with the username ‘%s’ already exists."
#~ msgstr "Een gebruiker met de gebruikersnaam ‘%s’ bestaat al."

#~ msgid "_Verify"
#~ msgstr "_Verifiëren"

#~ msgctxt "Password strength"
#~ msgid "Strength: Weak"
#~ msgstr "Sterkte: Zwak"

#~ msgctxt "Password strength"
#~ msgid "Strength: Low"
#~ msgstr "Sterkte: Laag"

#~ msgctxt "Password strength"
#~ msgid "Strength: Medium"
#~ msgstr "Sterkte: Middelsterk"

#~ msgctxt "Password strength"
#~ msgid "Strength: Good"
#~ msgstr "Sterkte: Goed"

#~ msgctxt "Password strength"
#~ msgid "Strength: High"
#~ msgstr "Sterkte: Hoog"

#~ msgid "Are these the right details? You can change them if you want."
#~ msgstr "Zijn dit de juiste gegevens? U kunt ze desgewenst wijzigen."

#~ msgid "You can review your online accounts (and add others) after setup."
#~ msgstr ""
#~ "U kunt uw online-accounts bekijken (en andere toevoegen) na het instellen."

#~ msgid "You're all set!"
#~ msgstr "U bent helemaal klaar!"

#~ msgid ""
#~ "We think that your time zone is %s. Press Next to continue or search for "
#~ "a city to manually set the time zone."
#~ msgstr ""
#~ "Wij denken dat uw tijdzone %s is. Druk op Volgende om verder te gaan of "
#~ "zoek naar een stad om de tijdzone handmatig in te stellen."

#~ msgid "Cancel"
#~ msgstr "Annuleren"

#~ msgid "Your username cannot be changed after setup."
#~ msgstr ""
#~ "Uw gebruikersnaam kan niet worden gewijzigd nadat de instellingen zijn "
#~ "voltooid."

#~ msgid ""
#~ "In order to use enterprise logins, this computer needs to be enrolled in "
#~ "the domain. Please have your network administrator type their domain "
#~ "password here, and choose a unique computer name for your computer."
#~ msgstr ""
#~ "Om bedrijfsaanmeldingen te gebruiken moet deze computer ingeschreven zijn "
#~ "bij het domein. Laat uw netwerkbeheerder hier het domeinwachtwoord "
#~ "invoeren en kies een unieke naam voor uw computer."

#~ msgid "No password"
#~ msgstr "Geen wachtwoord"

#~ msgctxt "Password strength"
#~ msgid "Too short"
#~ msgstr "Te kort"

#~ msgctxt "Password strength"
#~ msgid "Not good enough"
#~ msgstr "Niet goed genoeg"

#~ msgctxt "Password strength"
#~ msgid "Weak"
#~ msgstr "Zwak"

#~ msgctxt "Password strength"
#~ msgid "Fair"
#~ msgstr "Redelijk"

#~ msgctxt "Password strength"
#~ msgid "Good"
#~ msgstr "Goed"

#~ msgctxt "Password strength"
#~ msgid "Strong"
#~ msgstr "Sterk"

#~ msgid "Login"
#~ msgstr "Aanmelden"

#~ msgid "Create a Local Account"
#~ msgstr "Een lokaal account aanmaken"

#~ msgid "page 1"
#~ msgstr "pagina 1"

#~ msgid "Create an Enterprise Account"
#~ msgstr "Een bedrijfsaccount aanmaken"

#~ msgid "Other"
#~ msgstr "Overige"

#~ msgid "No input source selected"
#~ msgstr "Er zijn geen invoerbronnen geselecteerd"

#~ msgid "Add an Input Source"
#~ msgstr "Een invoerapparaat toevoegen"
